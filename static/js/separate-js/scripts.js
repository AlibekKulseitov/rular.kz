//'use strict';

$(function() {

    /*
    |--------------------------------------------------------------------------
    | Masked Input
    |--------------------------------------------------------------------------
    */

    $(".js-masked-phone").mask("+7 (999) 999-99-99");

  /*
  |--------------------------------------------------------------------------
  | Sidebar
  |--------------------------------------------------------------------------
  */

    var body = $('body');
    var burgerMenu = $('.js-sidebar-toggle');

    // Sidebar toggle to sidebar-folded
    burgerMenu.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        e.preventDefault();
        body.toggleClass('sidebar-open');
    });

    // Sidebar close
    $('.js-sidebar-close').on('click', function(e) {

        e.preventDefault();
        body.removeClass('sidebar-open');
    });

    /*
    |--------------------------------------------------------------------------
    | Userbar
    |--------------------------------------------------------------------------
    */

    var userMenu = $('.js-userbar-toggle');

    // UserMenu toggle to sidebar-folded
    userMenu.on('click', function(e) {

        // Toggle Open Class
        //$(this).toggleClass('is-open');

        e.preventDefault();
        body.toggleClass('userbar-open');
    });

    // UserMenu close
    $('.js-userbar-close').on('click', function(e) {

        e.preventDefault();
        body.removeClass('userbar-open');
    });

    /*
    |--------------------------------------------------------------------------
    | Showing modal with effect
    |--------------------------------------------------------------------------
    */

    $('.js-modal-effect').on('click', function(e) {

        e.preventDefault();

        var effect = $(this).attr('data-effect');
        $('.modal').addClass(effect);
    });
    // hide modal with effect

});
